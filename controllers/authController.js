const { User, GameRoom } = require('../models');
const passport = require('../lib/passport');

function format(user) {
    const { id, username } = user;

    return {
        id,
        username,
        accessToken: user.generateToken()
    }
}

module.exports = {
    // FRONTEND
    register: (req, res, next) => {
        User.register_fe(req.body)
        .then(() => {
            req.flash('success', 'Successfully create an account. Please login.');
            res.redirect('/login');
        })
        .catch(err => next(err));
    },
    login: passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: 'Username or Password not matching our credentials.'
    }),
    logout: (req, res) => {
        req.session.destroy((err) => {
            res.redirect('/');
        });
    },
    index: (req, res) => {
        const isAuthenticated = req.isAuthenticated();
        let user;
        if(isAuthenticated) {
            user = req.user.dataValues;
        } else {
            user = null;
        }
        console.log(user);
        res.render('index', {
            isAuthenticated: isAuthenticated,
            users: user
        });
    },

    // API
    registerApi: (req, res, next) => {
        User.register_api(req.body)
        .then(user => {
            res.json({
                'status': 'success',
                'message': 'Register berhasil, silahkan login.',
                'data': user
            }, 200);
        })
        .catch(err => {
            res.json({
                'status': 'error',
                'message': 'Periksa kembali data register anda.',
            }, 500);
        });
    },
    loginApi: (req, res) => {
        User.authenticate(req.body)
        .then(user => {
            res.json({
                'status': 'success',
                'message': 'Anda berhasil login',
                'data': format(user)
            });
        })
        .catch(err => {
            res.json({
                'status': 'error',
                'message': err
            }, 500);
        });
    },
    roomPage: (req, res) => {
        const currentUser = req.user;
        res.json(currentUser);
    },
    createRoom: (req, res, next) => {
        const roomName = req.body.roomName;
        const createdBy = req.user.id;
        
        GameRoom.createRoom({
            roomName, createdBy
        })
        .then(createRoom => {
            res.json({
                'status': 'success',
                'message': 'Game Room berhasil dibuat',
                'data': createRoom
            })
        })
        .catch(err => {
            res.json({
                'status': 'error',
                'message': err
            }, 500);
        });
    }
}