var express = require('express');
var router = express.Router();
const auth = require('../controllers/authController');
const restrict = require('../middlewares/restrict');

/* GET home page. */
router.get('/', auth.index);
router.get('/register', (req, res) => res.render('register'));
router.post('/register', auth.register);
router.get('/login', (req, res) => res.render('login'));
router.post('/login', auth.login);
router.get('/logout', auth.logout);

router.get('/game', restrict, (req, res) => {
    res.render('game');
});

module.exports = router;
